Pod::Spec.new do |s|
  s.name         = "BIFPIPViewController"
  s.version      = "4.0.3"
  s.summary      = "Creating a PIP experience with custom UIViewControllers"

  s.description  = <<-DESC
  Tries to mimic the PIP behaviour introduced in iOS9. Now you can use your viewcontrollers to create a PIP experience
                   DESC

  s.homepage     = "https://bitbucket.org/bitsfabrik/bifpipviewcontroller"
  s.license      = "MIT"
  s.author             = { "Philip Messlehner" => "mess.philip@gmail.com" }
  s.platform     = :ios, "9.0"
  s.source       = { :git => "git@bitbucket.org:bitsfabrik/bifpipviewcontroller.git", :tag => "#{s.version}" }
  s.source_files  = "Sources/**/*.{h,m,swift}"

  s.dependency "SnapKit"
end
