//
//  UIViewController.swift
//  Pods
//
//  Created by Philip Messlehner on 19.06.16.
//
//

import UIKit

public extension UIViewController {
    @objc(bif_pipViewController)
    public var pipViewController: PIPViewController? {
        if self is PIPViewController {
            return self as? PIPViewController
        }
        if let pipViewController = self.parent?.pipViewController {
            return pipViewController
        }
        return self.presentingViewController as? PIPViewController
    }
}
