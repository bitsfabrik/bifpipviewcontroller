//
//  PIPViewController.swift
//  Pods
//
//  Created by Philip Messlehner on 19.06.16.
//
//

import UIKit

///////////////////////////////////////////////////////////////////////////
// MARK: - BIFPIPViewController Constants -
///////////////////////////////////////////////////////////////////////////

public enum Notifications: String {
    case presentationStyleDidChange
    case presentationStyleWillChange
    
    case presentationStyleKey
}

///////////////////////////////////////////////////////////////////////////
// MARK: - PIPViewControllerPresentationStyle -
///////////////////////////////////////////////////////////////////////////

public enum PIPViewControllerPresentationStyle: Int {
    case pip
    case fullScreen
}

///////////////////////////////////////////////////////////////////////////
// MARK: - PIPHorizontalSnap -
///////////////////////////////////////////////////////////////////////////

public enum PIPHorizontalSnap {
    case right
    case left
}

///////////////////////////////////////////////////////////////////////////
// MARK: - PIPVerticalSnap -
///////////////////////////////////////////////////////////////////////////


public enum PIPVerticalSnap {
    case bottom
    case top
}

///////////////////////////////////////////////////////////////////////////
// MARK: - PIPViewController -
///////////////////////////////////////////////////////////////////////////

open class PIPViewController: UIViewController, UIGestureRecognizerDelegate {
    
    ///////////////////////////////////////////////////////////////////////////
    // MARK: Properties
    
    open fileprivate(set) weak var rootViewController: UIViewController? = nil {
        didSet {
            self.contentView.isHidden = self.rootViewController == nil || self.presentationStyle == .fullScreen
            self.setNeedsStatusBarAppearanceUpdate()
        }
    }
    
    open var presentationStyle: PIPViewControllerPresentationStyle {
        get {
            return self.internalPresentationStyle
        }
        set {
            self.setPresentationStyle(newValue, animated: false)
        }
    }
    
    open var inset: UIEdgeInsets {
        didSet {
            self.view.setNeedsLayout()
        }
    }
    open var horizontalSnap: PIPHorizontalSnap {
        didSet {
            self.view.setNeedsLayout()
        }
    }
    open var verticalSnap: PIPVerticalSnap {
        didSet {
            self.view.setNeedsLayout()
        }
    }
    open var size: CGSize
    open var minimumSize: CGSize
    
    public let contentView: UIView = UIView()
    
    ///////////////////////////////////////////////////////////////////////////
    // MARK: Private Properties
    
    fileprivate var internalPresentationStyle: PIPViewControllerPresentationStyle {
        didSet {
            guard self.presentationStyle != oldValue else {
                return
            }
            self.contentView.isHidden = self.rootViewController == nil || self.presentationStyle == .fullScreen
            self.setNeedsStatusBarAppearanceUpdate()
            self.view.setNeedsLayout()
            self.updateGestureRecognizers()
        }
    }
    
    fileprivate var panGestureRecognizer: UIPanGestureRecognizer!
    fileprivate var pinchGestureRecognizer: UIPinchGestureRecognizer!
    
    fileprivate var lastPanGestureLocation: CGPoint!
    fileprivate var initialSize: CGSize!
    
    ///////////////////////////////////////////////////////////////////////////
    // MARK: Life Cycle
    
    public required init() {
        self.internalPresentationStyle = .fullScreen
        self.inset = UIEdgeInsets(top: 20.0, left: 20.0, bottom: 20.0, right: 20.0)
        self.horizontalSnap = .right
        self.verticalSnap = .bottom
        self.size = CGSize(width: 208.0, height: 117.0)
        self.minimumSize = CGSize(width: 152, height: 85.5)
        
        super.init(nibName: nil, bundle: nil)
        self.contentView.frame = self.frameForPresentationStyle(self.presentationStyle)
        self.contentView.backgroundColor = UIColor.black
        self.contentView.layer.shadowRadius = 5.0
        self.contentView.layer.shadowColor = UIColor.black.cgColor
        self.contentView.layer.shadowOffset = CGSize(width: 0, height: 0.0)
        self.contentView.layer.shadowOpacity = 0.5
        self.contentView.clipsToBounds = false
        
        self.view.addSubview(self.contentView)
        
        self.panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        self.pinchGestureRecognizer = UIPinchGestureRecognizer(target: self, action: #selector(handlePinchGesture(_:)))
        
        self.contentView.addGestureRecognizer(self.panGestureRecognizer)
        self.contentView.addGestureRecognizer(self.pinchGestureRecognizer)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override open func loadView() {
        self.view = PassthroughView()
    }
    
    override open var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        if let viewController = UIApplication.shared.keyWindow?.rootViewController, !(viewController is PIPViewController) {
            return viewController.supportedInterfaceOrientations
        }
        return super.supportedInterfaceOrientations
    }
    
    open override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if self.presentationStyle == .pip {
            self.contentView.frame = self.frameForPresentationStyle(self.presentationStyle)
        }
    }
    
    open override var childForStatusBarStyle : UIViewController? {
		if self.presentationStyle == .fullScreen && self.rootViewController != nil  {
	    	return self.rootViewController
	    }
	    if let windowRootViewController = UIApplication.shared.keyWindow?.rootViewController, windowRootViewController != self {
	    	return windowRootViewController
	    }
		return nil
	}
    
    ///////////////////////////////////////////////////////////////////////////
    // MARK: - PIPViewController
    ///////////////////////////////////////////////////////////////////////////
    
    open override func present(_ viewControllerToPresent: UIViewController, animated: Bool, completion: (() -> Void)?) {
        guard self.rootViewController != viewControllerToPresent else {
            completion?()
            return
        }
        
        let presentationBlock: (() -> Void) = { () in
            self.rootViewController = viewControllerToPresent
            if self.presentationStyle == .fullScreen {
                viewControllerToPresent.modalPresentationStyle = .fullScreen
                super.present(viewControllerToPresent, animated: animated, completion: completion)
            } else {
                self.addChild(self.rootViewController!)
                self.contentView.frame = CGRect.zero
                self.rootViewController!.view.frame = self.contentView.bounds
                self.contentView.addSubview(self.rootViewController!.view)
                self.rootViewController!.didMove(toParent: self)
                self.updateGestureRecognizers()
                
                let frame = self.offScreenFrameForPresentationStyle(self.presentationStyle)
                if animated {
                    self.contentView.frame = self.offScreenFrameForPresentationStyle(self.presentationStyle)
                    UIView.animate(withDuration: 0.25, animations: {
                        self.contentView.frame = self.frameForPresentationStyle(self.presentationStyle)
                        }, completion: { (_) in
                            completion?()
                    })
                } else {
                    self.contentView.frame = frame
                    completion?()
                }
            }
        }
        
        if self.rootViewController != nil {
            self.dismissRootViewControllerAnimated(animated, completion: {
                presentationBlock()
            })
        } else {
            presentationBlock()
        }
    }
    
    open func dismissRootViewControllerAnimated(_ animated: Bool, completion: (() -> Void)?) {
        guard let rootViewController = self.rootViewController else {
            return
        }
        
        let adjustedCompletion: (() -> Void) = { () in
            completion?()
            self.rootViewController = nil
        }
        
        if rootViewController.presentingViewController == self {
            self.dismiss(animated: animated, completion: adjustedCompletion)
        } else {
            let removalBlock: (() -> Void) = { () in
                rootViewController.willMove(toParent: nil)
                rootViewController.view.removeFromSuperview()
                rootViewController.removeFromParent()
            }
            
            if animated {
                UIView.animate(withDuration: 0.25, animations: {
                    self.contentView.frame = self.offScreenFrameForPresentationStyle(self.presentationStyle)
                    }, completion: { (_) in
                        removalBlock()
                        adjustedCompletion()
                })
            } else {
                removalBlock()
                adjustedCompletion()
            }
        }
        
    }
    
    open func setPresentationStyle(_ presentationStyle: PIPViewControllerPresentationStyle, animated: Bool, completion: (() -> Void)? = nil) {
        guard let rootViewController = self.rootViewController, self.presentationStyle != presentationStyle else {
            self.internalPresentationStyle = presentationStyle
            return
        }
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: Notifications.presentationStyleWillChange.rawValue), object: rootViewController, userInfo: [Notifications.presentationStyleKey.rawValue: presentationStyle.rawValue])
        self.dismissRootViewControllerAnimated(animated) {
            self.internalPresentationStyle = presentationStyle
            let dispatchTime = DispatchTime.now() + Double(Int64(0.1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
                self.present(rootViewController, animated: animated) { () in
                    NotificationCenter.default.post(name: Notification.Name(rawValue: Notifications.presentationStyleDidChange.rawValue), object: rootViewController, userInfo: [Notifications.presentationStyleKey.rawValue: presentationStyle.rawValue])
                    completion?()
                }
            })
        }
    }
    
    ///////////////////////////////////////////////////////////////////////////
    // MARK: - PIPVewController - private Methods
    ///////////////////////////////////////////////////////////////////////////
    
    fileprivate func updateGestureRecognizers() {
        let gestureRecognizerEnabled = self.presentationStyle != .fullScreen
        self.panGestureRecognizer.isEnabled = gestureRecognizerEnabled
        self.pinchGestureRecognizer.isEnabled = gestureRecognizerEnabled
    }
    
    fileprivate func frameForPresentationStyle(_ presentationStyle: PIPViewControllerPresentationStyle) -> CGRect {
        if presentationStyle == .fullScreen {
            return self.view.bounds
        } else {
            let frame = self.view.bounds.inset(by: self.inset)
            var maximumSize = CGSize.zero
            maximumSize.width = min(min(frame.size.width, frame.size.height), 500)
            maximumSize.height = maximumSize.width/16*9.0
            
            if self.size.width < self.minimumSize.width || self.size.height < self.minimumSize.height {
                self.size = self.minimumSize
            }
            if self.size.width > maximumSize.width || self.size.height > maximumSize.height {
                self.size = maximumSize
            }
            
            var offsetY: CGFloat = frame.minY
            if self.verticalSnap == .bottom {
                offsetY = frame.maxY - self.size.height
            }
            var offsetX: CGFloat = frame.minX
            if self.horizontalSnap == .right {
                offsetX = frame.maxX - self.size.width
            }
            return CGRect(origin: CGPoint(x: offsetX, y: offsetY), size: self.size)
        }
    }
    
    fileprivate func offScreenFrameForPresentationStyle(_ presentationStyle: PIPViewControllerPresentationStyle) -> CGRect {
        guard presentationStyle != .fullScreen else {
            return CGRect.zero
        }
        
        var frame = self.frameForPresentationStyle(presentationStyle)
        if self.horizontalSnap == .left {
            frame.origin = CGPoint(x: 0 - self.inset.left - frame.size.width, y: frame.origin.y)
        } else {
            frame.origin = CGPoint(x: self.view.bounds.width + frame.size.width, y: frame.origin.y)
        }
        return frame
    }
    
    @objc fileprivate func handlePanGesture(_ gestureRecognizer: UIPanGestureRecognizer) {
        let newLocation = gestureRecognizer.location(in: self.view)
        guard self.rootViewController != nil else {
            return
        }
        
        switch gestureRecognizer.state {
        case .changed:
            var center = self.contentView.center
            center.x += newLocation.x - self.lastPanGestureLocation.x
            center.y += newLocation.y - self.lastPanGestureLocation.y
            self.contentView.center = center
            break
        case .ended, .failed, .cancelled:
            let currentContentViewFrame = self.contentView.frame
            let intersection = currentContentViewFrame.intersection(self.view.bounds)
            if intersection.width < currentContentViewFrame.width*0.40 {
                self.dismissRootViewControllerAnimated(true, completion: nil)
            } else {
                let rect = self.view.bounds.inset(by: self.inset)
                var center = newLocation
                center.x -= self.inset.left
                center.y -= self.inset.top
                if center.x <= rect.width/2.0 {
                    self.horizontalSnap = .left
                } else {
                    self.horizontalSnap = .right
                }
                if center.y <= rect.height/2.0 {
                    self.verticalSnap = .top
                } else {
                    self.verticalSnap = .bottom
                }
                UIView.animate(withDuration: 0.5, animations: {
                    self.contentView.frame = self.frameForPresentationStyle(self.presentationStyle)
                })
            }
            break
        default:
            break
        }
        self.lastPanGestureLocation = gestureRecognizer.location(in: self.view)
    }
    
    @objc fileprivate func handlePinchGesture(_ gestureRecognizer: UIPinchGestureRecognizer) {
        guard self.rootViewController != nil else {
            return
        }
        
        switch gestureRecognizer.state {
        case .began:
            self.initialSize = self.size
            break
        case .changed:
            if var newSize = self.initialSize {
                newSize.width = newSize.width * gestureRecognizer.scale
                newSize.height = newSize.height * gestureRecognizer.scale
                self.size = newSize
            }
            
            self.contentView.frame = self.frameForPresentationStyle(self.presentationStyle)
            break
        case .ended, .failed, .cancelled:
            self.view.setNeedsLayout()
            break
        default: break
        }
    }
    
    ///////////////////////////////////////////////////////////////////////////
    // MARK: - UIGestureRecognizerDelegate
    ///////////////////////////////////////////////////////////////////////////
    
    open func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRequireFailureOf otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer == self.pinchGestureRecognizer && otherGestureRecognizer == self.panGestureRecognizer {
            return true
        }
        return false
    }
    
    ///////////////////////////////////////////////////////////////////////////
    // MARK: - PasstrhoughView -
    ///////////////////////////////////////////////////////////////////////////
    
    fileprivate class PassthroughView: UIView {
        override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
            for view in self.subviews {
                let point = view.convert(point, from: self)
                if view.point(inside: point, with: event) {
                    return true
                }
            }
            return false
        }
    }
}
