//
//  Window.swift
//  Pods
//
//  Created by Philip Messlehner on 19.06.16.
//
//

import UIKit

///////////////////////////////////////////////////////////////////////////
// MARK: - PIPWindow -
///////////////////////////////////////////////////////////////////////////

open class PIPWindow: UIWindow {

    public static let sharedInstance = PIPWindow()

    ///////////////////////////////////////////////////////////////////////////
    // MARK: Life Cycle

    public required init(rootViewController: PIPViewController = PIPViewController()) {
        super.init(frame: UIScreen.main.bounds)
        self.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.windowLevel = .alert - 1
        self.isHidden = false
        self.rootViewController = rootViewController
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyWindowNotification), name: UIWindow.didBecomeKeyNotification, object: self)
    }

    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIWindow.didBecomeKeyNotification, object: self)
    }

    ///////////////////////////////////////////////////////////////////////////
    // MARK: - UIWindow
    ///////////////////////////////////////////////////////////////////////////

    override open func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        for view in self.subviews {
            let point = view.convert(point, from: self)
            if view.point(inside: point, with: event) {
                return true
            }
        }
        return false
    }

    ///////////////////////////////////////////////////////////////////////////
    // MARK: - PIPWindow
    ///////////////////////////////////////////////////////////////////////////

    public static func presentViewController(_ viewControllerToPresent: UIViewController, animated: Bool, completion: (() -> Void)?) {
        guard let rootViewController = PIPWindow.sharedInstance.rootViewController as? PIPViewController else {
            return
        }
        rootViewController.present(viewControllerToPresent, animated: animated, completion: completion)
    }

    public static func dismissViewControllerAnimated(_ animated: Bool, completion: (() -> Void)?) {
        guard let rootViewController = PIPWindow.sharedInstance.rootViewController as? PIPViewController else {
            return
        }
        rootViewController.dismissRootViewControllerAnimated(true, completion: completion)
    }

    public static var presentedViewController: UIViewController? {
        return self.pipViewController?.rootViewController
    }

    public static var pipViewController: PIPViewController? {
        return PIPWindow.sharedInstance.rootViewController as? PIPViewController
    }

    @objc fileprivate func handleKeyWindowNotification() {
        UIApplication.shared.delegate?.window??.makeKey()
    }
}
