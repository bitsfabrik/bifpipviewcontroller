//
//  ViewController.swift
//  BIFPIPViewControllerExample
//
//  Created by Philip Messlehner on 15.05.16.
//  Copyright © 2016 bitsfabrik GmbH. All rights reserved.
//

import UIKit
import BIFPIPViewController

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItems = [
            UIBarButtonItem(barButtonSystemItem: .Play, target: self, action: #selector(ViewController.presentSmallViewController(_:))),
            UIBarButtonItem(barButtonSystemItem: .FastForward, target: self, action: #selector(ViewController.pushViewController(_:)))
        ]
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return [.Portrait, .PortraitUpsideDown]
    }
    
    @objc func pushViewController(sender: AnyObject) {
        self.navigationController?.pushViewController(ViewController(), animated: true)
    }
    
    @objc func presentSmallViewController(sender: AnyObject) {
        if PIPWindow.presentedViewController == nil {
            PIPWindow.presentViewController(SmallViewController(), animated: true, completion: nil)
        } else {
            PIPWindow.dismissViewControllerAnimated(true, completion: nil)
        }
    }
}

