//
//  NavigationController.swift
//  BIFPIPViewControllerExample
//
//  Created by Philip Messlehner on 19.05.16.
//  Copyright © 2016 bitsfabrik GmbH. All rights reserved.
//

import UIKit
import BIFPIPViewController

class NavigationController: UINavigationController {
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return UIInterfaceOrientationMask.Portrait
        }
        
        return UIInterfaceOrientationMask.AllButUpsideDown
    }
}