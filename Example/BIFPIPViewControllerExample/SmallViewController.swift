//
//  SmallViewController.swift
//  BIFViewContollerInViewControllerPresentationControllerExample
//
//  Created by Philip Messlehner on 15.05.16.
//  Copyright © 2016 bitsfabrik GmbH. All rights reserved.
//

import UIKit
import SnapKit
import BIFPIPViewController

class SmallViewController: UIViewController {
    let label: UILabel = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.redColor()
        self.view.userInteractionEnabled = true
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTapGesture)))
        
        self.label.text = "Test"
        self.label.textAlignment = .Center
        self.view.addSubview(self.label)
        self.view.setNeedsUpdateConstraints()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        self.label.snp_makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return .Landscape
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }
    
    func handleTapGesture() {
        guard let pipViewController = self.pipViewController else {
            return
        }
        if pipViewController.presentationStyle == .FullScreen {
            pipViewController.setPresentationStyle(.PIP, animated: true)
        } else {
            pipViewController.setPresentationStyle(.FullScreen, animated: true)
        }
    }
}
