//
//  AppDelegate.swift
//  BIFPIPViewControllerExample
//
//  Created by Philip Messlehner on 15.05.16.
//  Copyright © 2016 bitsfabrik GmbH. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var pipWindow: UIWindow!

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {

//        self.pipWindow?.makeKeyAndVisible()
        self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        self.window?.rootViewController = NavigationController(rootViewController: ViewController())
        self.window?.backgroundColor = UIColor.whiteColor();
        self.window?.makeKeyAndVisible()
        
        return true
    }
}

